var webpack = require('webpack');

const path = require('path');

module.exports = {
  entry: {
    bolt: './bolt.jsx',
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js'
  },
  resolve: {
    extensions: ['.jsx', '.js', '.ts'],
    alias: {
      'preact': path.resolve(__dirname, './preact/preact')
    }
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    })
  ],
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 3000
  },
  module: {
    rules: [{
        test: /\.ts$/,
        use: {
          loader: 'awesome-typescript-loader'
        }
      },
      {
        test: /\.js|jsx$/,
        exclude: /node_modules|dist/,
        loader: "babel-loader"
      },
      {
        test: /\.html$/,
        exclude: /node_modules|dist/,
        loader: 'html-loader'
      }
    ]
  },
  devtool: 'source-map'
};